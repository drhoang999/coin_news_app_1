class SahaBanner {
  SahaBanner({
    this.id,
    this.imageUrl,
    this.title,
  });

  int? id;
  String? imageUrl;
  String? title;

  factory SahaBanner.fromJson(Map<String, dynamic> json) => SahaBanner(
        id: json["id"] == null ? null : json["id"],
        imageUrl: json["image_url"] == null ? null : json["image_url"],
        title: json["title"] == null ? null : json["title"],
      );

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "image_url": imageUrl == null ? null : imageUrl,
        "title": title == null ? null : title,
      };
}
