import 'package:flutter/material.dart';

ThemeData SahaUserPrimaryTheme = ThemeData(
  primarySwatch: Colors.teal,
  visualDensity: VisualDensity.adaptivePlatformDensity,
);
