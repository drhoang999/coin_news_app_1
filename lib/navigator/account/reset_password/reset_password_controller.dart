import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:coinnews12h/component/toast/saha_alert.dart';
import 'package:coinnews12h/data/repository/repository_manager.dart';

class ResetPasswordController extends GetxController {
  var resting = false.obs;
  var newPassInputting = false.obs;
  var checkingHasPhone = false.obs;

  var otp = "";

  TextEditingController textEditingControllerNumberPhone =
      new TextEditingController();

  TextEditingController textEditingControllerNewPass =
      new TextEditingController();

  Future<void> onReset() async {
    resting.value = true;
    try {
      var loginData = (await RepositoryManager.loginRepository.resetPassword(
          phone: textEditingControllerNumberPhone.text,
          pass: textEditingControllerNewPass.text,
          otp: otp))!;

      SahaAlert.showSuccess(
          message: "Password reset successful, please login again");
      Get.back(result: {
        "success": true,
        "phone": textEditingControllerNumberPhone.text,
        "pass": textEditingControllerNewPass.text
      });
      resting.value = false;
    } catch (err) {
      SahaAlert.showError(message: err.toString());
      resting.value = false;
    }
    resting.value = false;
  }

  Future<void> checkHasPhoneNumber({Function? onHas, Function? noHas}) async {
    checkingHasPhone.value = true;
    try {
      var data = await RepositoryManager.loginRepository.checkExists(
        phoneNumber: textEditingControllerNumberPhone.text,
      );

      for (var e in data!) {
        if (e.name == "phone_number" && e.value == true) {
          if (onHas != null) onHas();
          checkingHasPhone.value = false;
          return;
        }
      }
      SahaAlert.showError(message: "Số điện thoại không tồn tại");
      if (noHas != null) noHas();
    } catch (err) {
      SahaAlert.showError(message: err.toString());
    }
    checkingHasPhone.value = false;
  }
}
