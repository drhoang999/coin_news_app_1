import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinnews12h/component/button/saha_button.dart';
import 'package:coinnews12h/component/loading/loading_full_screen.dart';
import 'package:coinnews12h/component/text_field/sahashopTextField.dart';

import 'form_bonus_controller.dart';

class FormBonusScreen extends StatelessWidget {
  FormBonusController formBonusController = new FormBonusController();

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Nhận thưởng"),
      ),
      body: Obx(
        () => Form(
          key: _formKey,
          child: Stack(
            children: [
              SingleChildScrollView(
                child: Column(
                  children: [
                    if (formBonusController.sent.value)
                      Container(
                        margin: EdgeInsets.all(8),
                        padding: EdgeInsets.all(8),
                        child: Text(formBonusController.showSuccess,
                          style: TextStyle(color: Colors.white),
                        ),
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.all(Radius.circular(8)),
                            border: Border.all(color: Colors.white)),
                      ),
                    Divider(),
                    SahaTextField(
                      labelText: "Số điện thoại",
                      textInputType: TextInputType.phone,
                      controller:
                          formBonusController.textEditingControllerPhone,
                      validator: (value) {
                        if (value!.length == 0) {
                          return 'Không được để trống';
                        }
                        return null;
                      },
                    ),
                    Divider(),
                    SahaTextField(
                      labelText: "Email",
                      controller:
                          formBonusController.textEditingControllerEmail,
                      validator: (value) {
                        if (value!.length == 0) {
                          return 'Không được để trống';
                        }
                        return null;
                      },
                      textInputType: TextInputType.emailAddress,
                    ),
                    Divider(),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 15, right: 15, top: 8),
                      child: TextFormField(
                        controller:
                            formBonusController.textEditingControllerAddress,
                        validator: (value) {
                          if (value!.length == 0) {
                            return 'Không được để trống';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                            labelText: "Địa chỉ ví",
                            helperText: "Địa chỉ ví nhận token ERC-20 của bạn",
                            labelStyle: TextStyle(fontSize: 12)),
                        maxLines: 3,
                      ),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Column(
                      children: [
                        SahaTextField(
                          labelText: "Mã OTP",
                          controller:
                              formBonusController.textEditingControllerOtp,
                          suffixWidget: null,
                          validator: (value) {
                            if (value!.length == 0) {
                              return 'Không được để trống';
                            }
                            return null;
                          },
                          textInputType: TextInputType.number,
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            if (formBonusController.isSentOtp.value == true)
                              Text(formBonusController.start.value.toString() +
                                  "s"),
                            if (formBonusController.isSentOtp.value == false)
                              InkWell(
                                child: Text(
                                  "Nhận mã OTP",
                                  style: TextStyle(fontWeight: FontWeight.bold),
                                ),
                                onTap: () {
                                  formBonusController.sendOtp();
                                },
                              ),
                            SizedBox(
                              width: 10,
                            )
                          ],
                        )
                      ],
                    ),

                    SizedBox(
                      height: 30,
                    ),
                    SahaButtonFullParent(
                      text: "Nhận thưởng",
                      onPressed: () {
                        FocusScope.of(context).requestFocus(FocusNode());

                        if (_formKey.currentState!.validate()) {
                          formBonusController.sendFormBonus();
                        }
                      },
                    )
                  ],
                ),
              ),
              formBonusController.sending.value
                  ? SahaLoadingFullScreen()
                  : Container()
            ],
          ),
        ),
      ),
    );
  }
}
