import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../speak_controller.dart';

class ControllerPlayWidget extends StatelessWidget {
  SpeakController speakController = Get.find();

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => Container(
        height: 50,
        width: Get.width,
        child: Row(
          children: [
            IconButton(
                onPressed: () {
                  speakController.playNewPost();
                },
                icon: Icon(Icons.play_arrow)),
            Row(
              children: [
                Text("00"),
                Slider(
                  min: 0,
                  max: 100,
                  value: 50,
                  onChanged: (value) {},
                ),
                Text("00"),
              ],
            ),
            Text(speakController.statusSpeak.value.toString())
          ],
        ),
      ),
    );
  }
}
