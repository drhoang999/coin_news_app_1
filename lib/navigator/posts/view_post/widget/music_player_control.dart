import 'dart:async';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:get/get.dart';
import 'package:ionicons/ionicons.dart';
import 'package:lottie/lottie.dart';
import 'package:marquee/marquee.dart';
import '../../../../speak_controller.dart';
import 'music_player_slider.dart';

const play_music_height = 50.0;

class MusicPlayerControl extends StatefulWidget {
  final bool? isWaitingNext;
  final bool? isPlaying;
  final Function? onDiskPress;
  final Function? onNext;
  final Function? onClose;
  final Function? onSeek;
  final Function? onStop;
  final Function? onSetRepeat;
  final Function? onSetShuffle;
  final Function? onOffVolume;
  final Function? onChangeVolume;
  final Function? onPlay;
  final Function? onPause;

  const MusicPlayerControl(
      {Key? key,
      this.isWaitingNext = false,
      this.isPlaying = true,
      this.onDiskPress,
      this.onNext,
      this.onStop,
      this.onClose,
      this.onSeek,
      this.onSetRepeat,
      this.onSetShuffle,
      this.onOffVolume,
      this.onChangeVolume,
      this.onPlay,
      this.onPause})
      : super(key: key);
  @override
  _MusicPlayerControlState createState() => _MusicPlayerControlState();
}

class _MusicPlayerControlState extends State<MusicPlayerControl>
    with TickerProviderStateMixin {
  SpeakController speakController = Get.find();

  bool isPlaying = true;
  bool isBuffering = false;
  bool isShowOption = false;

  bool isOffVolume = false;

  int duration = 0;
  int currentPosition = 0;

  int indexListVideo = -1;
  List listVideo = [];
  String youtubeUrl = "";
  bool isGetMp3Youtube = false;
  bool changingPosition = false;

  double volume = 0;
  var theme;

  var store;
  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget sliderProcess() {
    return Obx(
      () =>MusicSliderWidget(
        //value:(duration/2).floor(),
        value:
        speakController.miliCurrent.value <= speakController.miliMax.value
            ? speakController.miliCurrent.value
            : 0,
        min: 0,
        max: speakController.miliMax.value,
        fullWidth: true,
        sliderHeight: 15,
        onChangeStart: (value) {
          changingPosition = true;
        },
        onChange: (value) {
          currentPosition = value;

          Duration duration = Duration(milliseconds: (currentPosition));
          speakController.onSeek(duration);
        },
        onChangeEnd: (value) {
          currentPosition = value;

          Duration duration = Duration(milliseconds: (currentPosition));
          speakController.onSeek(duration);

          // widget.onSeek!(duration, () {
          //   changingPosition = false;
          // });
        },
      ),
    );
  }

  Widget _buildContent() {
    return Container(
      height: 50,
      width: Get.width,
      color: Colors.black.withOpacity(0.8),
      child: Obx(
        () => Row(
          children: <Widget>[
            Stack(
              children: [
                Container(
                  child: CachedNetworkImage(
                    imageUrl: speakController.post!.thumbnai!,
                    width: 50 + 20,
                    height: 50 - 8,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  top: 0,
                  left: 0,
                  right: 0,
                  bottom: 0,
                  child: Container(
                    alignment: Alignment.center,
                    color: Color.fromRGBO(1, 1, 1, 0.2),
                    child:
                        speakController.statusSpeak.value == StatusSpeak.Loading
                            ? Lottie.asset(
                                "assets/json/soundwave.json",
                                width: play_music_height * 0.6,
                                height: play_music_height * 0.6,
                              )
                            : InkWell(
                                child: Icon(
                                  speakController.statusSpeak.value ==
                                          StatusSpeak.Playing
                                      ? Icons.pause_circle_outline_outlined
                                      : Icons.play_circle_fill,
                                  color: Colors.white,
                                  size: play_music_height * 0.6,
                                ),
                                onTap: () {
                                  widget.onDiskPress!();
                                },
                              ),
                  ),
                )
              ],
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.center,
                child: Container(
                  height: play_music_height,
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Flexible(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SizedBox(
                              height: 5,
                            ),
                            Expanded(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      children: [
                                        Text(
                              "Nghe đọc báo tự động",
                              style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.white,
                              ),
                            ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Icon(Ionicons.volume_high_outline, size: 15,color: Colors.white,)
                                      ],
                                    ),
                                  ],
                                )),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 5),
                              child: SizedBox(
                                height: 18,
                                child: InkWell(
                                    onTap: () {},
                                    child: Obx(
                                      () => speakController.statusSpeak.value ==
                                              StatusSpeak.Playing
                                          ? Marquee(

                                              velocity: 30,
                                              text: speakController.post!.title! +
                                                  "    ",
                                              style: TextStyle(

                                                fontSize: 10.0,
                                                color: Colors.grey,
                                              ),
                                            )
                                          : Marquee(
                                              velocity: 0.00000001,
                                              text: speakController.post!.title! +
                                                  "    ",
                                              style: TextStyle(
                                                fontSize: 10.0,
                                                color: Colors.grey,
                                              ),
                                            ),
                                    ),

                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 15,
                      ),
                      Row(
                        children: <Widget>[
                          // widget.isWaitingNext == true || isGetMp3Youtube == true
                          //     ? SpinKitRipple(size: 30, color: Colors.white)
                          //     : IconButton(
                          //   icon: Icon(
                          //     Icons.skip_next,
                          //     size: 30,
                          //     color: Theme.of(context).primaryColor,
                          //   ),
                          //   onPressed: () {
                          //     widget.onNext!();
                          //   },
                          // ),
                          IconButton(
                            icon: Icon(
                              Icons.more_horiz,
                              color: Theme.of(context).primaryColor,
                            ),
                            onPressed: () {
                              showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: const Text("Chọn giọng đọc"),
                                    content: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        InkWell(
                                          child: Container(
                                            child: const Text("Nam"),
                                            padding: EdgeInsets.all(8),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.grey),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8))),
                                          ),
                                          onTap: () {
                                            speakController
                                                .onChooseGender("MALE");
                                            Get.back();
                                          },
                                        ),
                                        SizedBox(
                                          width: 15,
                                        ),
                                        InkWell(
                                          child: Container(
                                            child: const Text("Nữ"),
                                            padding: EdgeInsets.all(8),
                                            decoration: BoxDecoration(
                                                border: Border.all(
                                                    color: Colors.grey),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8))),
                                          ),
                                          onTap: () {
                                            speakController
                                                .onChooseGender("FEMALE");
                                            Get.back();
                                          },
                                        ),
                                      ],
                                    ),
                                    // actions: [
                                    //   new FlatButton(
                                    //     child: const Text("Ok"),
                                    //     onPressed: () => Navigator.pop(context),
                                    //   ),
                                    // ],
                                  );
                                },
                              );
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        alignment: Alignment.bottomLeft,
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 5,
                ),
                Column(
                  children: [
                    _buildContent(),
                  ],
                )
              ],
            ),
          ),
          Align(alignment: Alignment.topLeft, child: sliderProcess()),
        ],
      ),
    );
  }
}

typedef void OnDragEndCallBack(double dx, dy);
