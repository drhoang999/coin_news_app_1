
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:html/parser.dart' as htmlparser;
import 'package:html/dom.dart' as dom;
import 'package:ionicons/ionicons.dart';
import 'package:coinnews12h/admob/admob_controller.dart';
import 'package:coinnews12h/admob/widget_item_admob_list.dart';
import 'package:coinnews12h/component/loading/loading_container.dart';
import 'package:coinnews12h/component/loading/loading_full_screen.dart';
import 'package:coinnews12h/const/env.dart';
import 'package:coinnews12h/model/post.dart';

import '../../../speak_controller.dart';
import 'view_post_controller.dart';
import 'widget/controller_play.dart';
import 'widget/music_player_control.dart';

class ViewPostScreen extends StatefulWidget {
  Post? post;
  int? postId;

  late ViewPostController viewPostController;

  ViewPostScreen({Key? key, this.post, this.postId}) {
    viewPostController = ViewPostController(postId ?? post!.id!);
  }

  @override
  _ViewPostScreenState createState() => _ViewPostScreenState();
}

class _ViewPostScreenState extends State<ViewPostScreen> {
  SpeakController speakController = Get.find();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }


  @override
  void dispose() {
    speakController.onStop();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.post!.thumbnai);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.post !=null && widget.post!.category != null ? widget.post!.category! : ""),
        // title: CachedNetworkImage(
        //   height: 30,
        //   fit: BoxFit.cover,
        //   imageUrl: DOMAIN_API_CUSTOMER + "image/logo/tuoitre.png",
        //   placeholder: (context, url) => SahaLoadingContainer(
        //     height: 20,
        //     width: 50,
        //     gloss: 0.5,
        //   ),
        //   errorWidget: (context, url, error) => Container(
        //       height: 70, width: 120, color: Colors.grey.withOpacity(0.2)),
        // ),
        actions: [
          Obx(
            () => IconButton(
                onPressed: () {
                  widget.viewPostController
                      .favorite(!widget.viewPostController.isFavorite.value);
                },
                icon: Icon(!widget.viewPostController.isFavorite.value
                    ? Ionicons.bookmark_outline
                    : Ionicons.bookmark)),
          )
        ],
      ),
      body: Obx(
          () =>  widget.viewPostController.loading.value ? SahaLoadingFullScreen()  :  Column(
          children: [




            Expanded(
              child: SingleChildScrollView(
                child: Column(
                    children: widget.viewPostController.listDescription.map((e) {
                  return Column(
                    children: [
                      ClipRRect(
                        borderRadius: BorderRadius.circular(0.0),
                        child: CachedNetworkImage(
                          width: Get.width,
                          fit: BoxFit.contain,
                          imageUrl: widget.post!.thumbnai!,
                          placeholder: (context, url) => SahaLoadingContainer(
                            height: 70,
                            width: 120,
                            gloss: 0.5,
                          ),
                          errorWidget: (context, url, error) => Container(
                              height: 70,
                              width: 120,
                              color: Colors.grey.withOpacity(0.2)),
                        ),
                      ),
                      Html(
                          data: e,
                          onLinkTap: (String? url, RenderContext context,
                              Map<String, String> attributes, dom.Element? element) {
                            //open URL in webview, or launch URL in browser, or any other logic here
                          }),
                      ItemAdMob()
                    ],
                  );
                }).toList()),
              ),
            ),
            // MusicPlayerControl(
            //   onDiskPress: () {
            //     speakController.playNewPost();
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}
