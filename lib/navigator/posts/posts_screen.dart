import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:coinnews12h/navigator/symbol/symbol_screen.dart';
import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:coinnews12h/component/loading/loading_container.dart';
import 'package:coinnews12h/const/list_category.dart';
import 'package:coinnews12h/navigator/account/account.dart';
import 'package:coinnews12h/navigator/form_bonus/form_bonus.dart';
import 'package:coinnews12h/navigator/posts/post_page.dart';
import 'package:coinnews12h/navigator/setting/setting_screen.dart';

class PostsScreen extends StatefulWidget {
  @override
  _PostsScreenState createState() => _PostsScreenState();
}

class _PostsScreenState extends State<PostsScreen>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController =
        TabController(length: LIST_CATEGORY_POST.length, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      // Added
      length: LIST_CATEGORY_POST.length, // Added
      initialIndex: 0, //Added
      child: Scaffold(
        drawer: new Drawer(
          elevation: 0,
          child: Container(

            child: Column(
              children: [
                Container(
                  height: MediaQuery.of(context).padding.top,
                ),
                Expanded(
                  child: ListView.builder(
                      padding: EdgeInsets.zero,
                      itemCount: (LIST_CATEGORY_POST.length).floor(),
                      itemBuilder: (context, index) => Row(
                            children: [
                              Expanded(child: buildItemCate(index)),
                              // Expanded(child: buildItemCate(index * 2 + 1)),
                            ],
                          )),
                ),
                Container(
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: Container(
                                height: 50,
                                alignment: Alignment.center,
                                margin: EdgeInsets.all(8),
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color:Color(0xff085660),
                                  border: Border.all( color: Theme.of(context).primaryColor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(Ionicons.home_outline,  color: Colors.white),
                                    Text("Home" , style: TextStyle(
                                    color: Colors.white
                              ),),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SymbolScreen()));
                              },
                              child: Container(
                                height: 50,
                                alignment: Alignment.center,
                                margin: EdgeInsets.all(8),
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color:Color(0xff085660),
                                  border: Border.all(  color: Theme.of(context).primaryColor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(Ionicons.pricetag_outline,  color:Colors.white),
                                    Text("Prices",
                                    style: TextStyle(
                                      color:Colors.white
                                    ),),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => SettingScreen()));
                              },
                              child: Container(
                                height: 50,
                                alignment: Alignment.center,
                                margin: EdgeInsets.all(8),
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color:Color(0xff085660),
                                  border: Border.all( color: Theme.of(context).primaryColor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Text("Settings", style: TextStyle(
                                      color: Colors.white,
                                    ),),
                                    Icon(Ionicons.settings_outline,
                                    color: Colors.white,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: () {
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => AccountScreen()));
                              },
                              child: Container(
                                height: 50,
                                alignment: Alignment.center,
                                margin: EdgeInsets.all(8),
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(
                                  color:Color(0xff085660),
                                  border: Border.all( color: Theme.of(context).primaryColor),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(3)),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceAround,
                                  children: [
                                    Icon(Ionicons.person_outline,     color:Colors.white),
                                    Text("Account", style: TextStyle(
                                      color:Colors.white
                                    ),),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 15,
                )
              ],
            ),
          ),
        ),
        body: NestedScrollView(
          headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
            return <Widget>[
              SliverAppBar(
                  leading: Container(),
                  title: Text('News'),
                  pinned: true,
                  floating: true,
                  forceElevated: innerBoxIsScrolled,
                  bottom: PreferredSize(
                    child: Row(
                      children: [
                        Container(
                          color:Color(0xff085660),
                          child: Padding(
                            padding: const EdgeInsets.only(right: 5),
                            child: IconButton(
                                onPressed: () {
                                  Scaffold.of(context).openDrawer();
                                },
                                icon: Icon(
                                  Ionicons.list,
                                  color: Colors.white,
                                )),
                          ),
                        ),
                        Expanded(
                          child: Material(
                            color: Colors.white,
                            child: TabBar(
                              unselectedLabelColor: Colors.lightBlue[100],
                              labelColor: Theme.of(context).primaryColor,
                              isScrollable: true,
                              tabs: LIST_CATEGORY_POST
                                  .map(
                                    (e) => Tab(text: e.name.capitalize()),
                                  )
                                  .toList(),
                              controller: _tabController,
                            ),
                          ),
                        ),
                      ],
                    ),
                    preferredSize: Size.fromHeight(50),
                  )),
            ];
          },
          body: TabBarView(
            controller: _tabController,
            children: LIST_CATEGORY_POST
                .map((e) => Container(
                    key: new PageStorageKey(e.name),
                    child: PostPageWidget(
                      category: e.name,
                    )))
                .toList(),
          ),
        ),
      ),
    );
  }

  Widget buildItemCate(int index) {
    return InkWell(
      onTap: () {
        _tabController.animateTo(index);
        Navigator.pop(context);
      },
      child: Container(
        height: 70,

        alignment: Alignment.center,
        decoration: BoxDecoration(
          // color: Colors.black.withOpacity(0.5),
        ),
        child: Row(

          children: [
            Container(
              width: 25,

            ),
            CachedNetworkImage(
              fit: BoxFit.cover,
              height: 50,
              width: 50,
              imageBuilder: (context, imageProvider) => Container(
                width: 80.0,
                height: 80.0,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      image: imageProvider, fit: BoxFit.cover),
                ),
              ),
              imageUrl: LIST_CATEGORY_POST[index].image,
              placeholder: (context, url) => SahaLoadingContainer(
                height: 70,

                gloss: 0.5,
              ),
              errorWidget: (context, url, error) => Container(
                  height: 70, width: 70, color: Colors.grey.withOpacity(0.2)),
            ),
            Container(
              width: 25,

            ),
            Text(
              LIST_CATEGORY_POST[index].name.toUpperCase(),
              style:
                  TextStyle(fontWeight: FontWeight.bold, ),
            ),
          ],
        ),
      ),
    );
  }
}

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}
