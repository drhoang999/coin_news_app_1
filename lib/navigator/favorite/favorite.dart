import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinnews12h/component/loading/post_item_loading.dart';
import 'package:coinnews12h/navigator/posts/widget/post_widget.dart';
import 'favorite_controller.dart';


class FavoriteWidget extends StatefulWidget {
  final String? category;

  const FavoriteWidget({Key? key, this.category}) : super(key: key);
  @override
  _FavoriteWidgetState createState() => _FavoriteWidgetState();
}

class _FavoriteWidgetState extends State<FavoriteWidget>
    with AutomaticKeepAliveClientMixin<FavoriteWidget> {
  late FavoriteController favoriteController;
  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    favoriteController = new FavoriteController(widget.category);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        title: Text("Marked"),
      ),
      body: Obx(
            () => favoriteController.isLoadingPost.value == true
            ? ListView.builder(
            padding: EdgeInsets.zero,
            itemBuilder: (context, index) => PostItemLoadingWidget())
            : ListView.builder(
            padding: EdgeInsets.zero,
            itemCount: favoriteController.listPost.length,
            itemBuilder: (context, index) =>
                PostWidget(post: favoriteController.listPost[index])),
      ),
    );
  }
}
