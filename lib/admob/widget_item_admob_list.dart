import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'dart:io' show Platform;
import 'admob_controller.dart';

class ItemAdMob extends StatefulWidget {

  @override
  _ItemAdMobState createState() => _ItemAdMobState();
}

class _ItemAdMobState extends State<ItemAdMob> {

  BannerAd? bannerAd;


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // this.onLoadAd();
  }

  void onLoadAd() {
    bannerAd = BannerAd(
      adUnitId: Platform.isAndroid ? "ca-app-pub-9752477800312373/6860481960" : "ca-app-pub-9752477800312373/1671518860",
      request: AdRequest(),
      size: AdSize.banner,
      listener: BannerAdListener(
        onAdLoaded: (Ad ad) {
          print('$BannerAd loaded.');
        },
        onAdFailedToLoad: (Ad ad, LoadAdError error) {
          print('$BannerAd failedToLoad: $error');
        },
        onAdOpened: (Ad ad) => print('$BannerAd onAdOpened.'),
        onAdClosed: (Ad ad) => print('$BannerAd onAdClosed.'),
        onAdWillDismissScreen: (Ad ad) => print('$BannerAd onAdWillDismissScreen.'),
      ),
    );

    bannerAd?.load();

  }

  @override
  Widget build(BuildContext context) {

    return Container();

    // TODO: implement build
    return Container(
      alignment: Alignment.center,
      child: AdWidget(ad: bannerAd!),
      width: bannerAd!.size.width.toDouble(),
      height: bannerAd!.size.height.toDouble(),
    );
  }
}