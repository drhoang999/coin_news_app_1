import 'dart:async';
import 'package:coinnews12h/navigator/webview/webview.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:coinnews12h/utils/user_info.dart';

import 'data_app_controller.dart';
import 'navigator/navigator.dart';
import 'navigator/posts/posts_screen.dart';

class SahaMainScreen extends StatefulWidget {
  @override
  _SahaMainScreenState createState() => _SahaMainScreenState();
}

class _SahaMainScreenState extends State<SahaMainScreen> {
  @override
  void didChangeDependencies() {
    DataAppController dataAppController = Get.find();
    dataAppController.getThemeStatus();

    super.didChangeDependencies();
    loadInit(context);
  }

  void loadInit(BuildContext context) {
    checkLogin(context);
  }

  Future<void> checkLogin(BuildContext context) async {
    DataAppController dataAppController = Get.find();
    var link = await dataAppController.getLinkConfig();
    if (link != "") {
      Get.offAll(() => WebViewWidget(url: link));
    } else {
      noLogin(context);
    }
  }

  hasLogged(BuildContext context) {
    Get.offAll(() => PostsScreen());
    //Get.offAll(() => HomeScreen());
  }

  noLogin(BuildContext context) {
    Get.offAll(() => PostsScreen());
    //Get.offAll(() => LoginScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          color:Color(0xff085660),
            // color: Colors.white
            // gradient: LinearGradient(
            //   begin: Alignment.topRight,
            //   end: Alignment.bottomLeft,
            //   stops: [0.1, 0.5, 0.7, 0.9],
            //   colors: [
            //    SahaPrimaryColor,
            //     SahaPrimaryLightColor
            //   ],
            // ),
            ),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                // color: Colors.white,
                child: Image.asset(
                  "assets/logo.png",
                  height: 150,
                  fit: BoxFit.cover,
                  alignment: Alignment.center,
                ),
              ),
              SizedBox(
                height: 30,
              ),
              // Text(
              //   "Tinnhanh24h",
              //   style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
              // )
            ],
          ),
        ),
      ),
    );
  }
}
