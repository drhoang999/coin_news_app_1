import 'package:flutter/material.dart';

ThemeData darkGreen = ThemeData(
    accentColor: Colors.teal,
    brightness: Brightness.dark,
    primaryColor: Color(0xff085660),
    buttonTheme: ButtonThemeData(
      buttonColor: Color(0xff085660),
      disabledColor: Colors.grey,
    ));
ThemeData lightGreen = ThemeData(
    accentColor: Colors.tealAccent,
    primaryTextTheme: TextTheme(
        headline6: TextStyle(
            color: Colors.white
        ),
      subtitle2: TextStyle(
          color: Colors.white
      ),
    ),
    brightness: Brightness.light,
    primaryColor: Color(0xff085660),
    buttonTheme: ButtonThemeData(
      buttonColor: Color(0xff085660),
      disabledColor: Colors.grey,
    ));
ThemeData darkBlue = ThemeData(
    accentColor: Colors.teal,
    brightness: Brightness.dark,
    primaryColor: Colors.blue,
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.green,
      disabledColor: Colors.grey,
    ));
ThemeData lightBlue = ThemeData(
    accentColor: Colors.pink,
    brightness: Brightness.light,
    primaryColor: Colors.red,
    buttonTheme: ButtonThemeData(
      buttonColor: Colors.red,
      disabledColor: Colors.grey,
    ));

var LIST_THEME_DATA = [ lightGreen,darkGreen, darkBlue, lightBlue];
