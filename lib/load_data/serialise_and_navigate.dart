import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:get/get.dart';
import 'package:coinnews12h/main.dart';
import 'package:coinnews12h/navigator/posts/view_post/view_post.dart';

class SerialiseAndNavigate {
  RemoteMessage message;

  SerialiseAndNavigate({required this.message});

  void navigate() {
    var data = message.data;

    // String screen = data['screen'];
    // if (screen == "secondScreen") {
    //   Navigator.of(GlobalVariable.navState.currentState!.context)
    //       .pushNamed("secondScreen", arguments: data['post_id']);
    // } else if (screen == "thirdScreen") {
    //   Navigator.of(GlobalVariable.navState.currentState!.context).pushNamed("thirdScreen");
    // } else {
    //   //do nothing
    // }

    SchedulerBinding.instance!.addPostFrameCallback((_) {
      GlobalVariable.postId = int.tryParse(data['post_id'] ?? "1000");
      Navigator.push(GlobalVariable.navState.currentContext!,
          MaterialPageRoute(builder: (context) =>
              ViewPostScreen(
                  postId: int.tryParse(data['post_id'] ?? "1000"))));
    });
  }
}
