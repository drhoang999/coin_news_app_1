import 'package:coinnews12h/utils/user_info.dart';

class LoadLogin {
  static Future<void> load() async {
    await UserInfo().hasLogged();
  }
}
