import 'dart:convert';

ConfigLinkResponse configLinkResponseFromJson(String str) => ConfigLinkResponse.fromJson(json.decode(str));


class ConfigLinkResponse {
  ConfigLinkResponse({
    this.code,
    this.success,
    this.msgCode,
    this.msg,
    this.data,
  });

  int? code;
  bool? success;
  String? msgCode;
  String? msg;
  List<ConfigLink>? data;

  factory ConfigLinkResponse.fromJson(Map<String, dynamic> json) => ConfigLinkResponse(
    code: json["code"] == null ? null : json["code"],
    success: json["success"] == null ? null : json["success"],
    msgCode: json["msg_code"] == null ? null : json["msg_code"],
    msg: json["msg"] == null ? null : json["msg"],
    data: json["data"] == null ? null : List<ConfigLink>.from(json["data"].map((x) => ConfigLink.fromJson(x))),
  );


}

class ConfigLink {
  ConfigLink({
    this.id,
    this.codeApp,
    this.link,

  });

  int? id;
  String? codeApp;
  String? link;


  factory ConfigLink.fromJson(Map<String, dynamic> json) => ConfigLink(
    id: json["id"] == null ? null : json["id"],
    codeApp: json["code_app"] == null ? null : json["code_app"],
    link: json["link"] == null ? null : json["link"],

  );

  Map<String, dynamic> toJson() => {
    "id": id == null ? null : id,
    "code_app": codeApp == null ? null : codeApp,
    "link": link == null ? null : link,
  };
}
