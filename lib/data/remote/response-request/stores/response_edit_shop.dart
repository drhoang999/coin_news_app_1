import 'package:coinnews12h/model/store.dart';

class ResponseEditShop {
  ResponseEditShop({
    this.code,
    this.success,
    this.msgCode,
    this.msg,
    this.store,
  });

  int? code;
  bool? success;
  String? msgCode;
  String? msg;
  Store? store;

  factory ResponseEditShop.fromJson(Map<String, dynamic> json) =>
      ResponseEditShop(
        code: json["code"] == null ? null : json["code"],
        success: json["success"] == null ? null : json["success"],
        msgCode: json["msg_code"] == null ? null : json["msg_code"],
        msg: json["msg"] == null ? null : json["msg"],
        store: json["data"] == null ? null : Store.fromJson(json["data"]),
      );
}
