class ProfileResponse {
  int? code;
  bool? success;
  DataProfile? data;
  String? msgCode;
  String? msg;

  ProfileResponse({this.code, this.success, this.data, this.msgCode, this.msg});

  ProfileResponse.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    success = json['success'];
    data = json['data'] != null ? new DataProfile.fromJson(json['data']) : null;
    msgCode = json['msg_code'];
    msg = json['msg'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['msg_code'] = this.msgCode;
    data['msg'] = this.msg;
    return data;
  }
}

class DataProfile {
  String? areaCode;
  String? phoneNumber;
  String? email;
  String? name;
  int? points;
  int? sex;
  String? createdAt;
  String? updatedAt;

  DataProfile(
      {
        this.areaCode,
        this.phoneNumber,
        this.email,
        this.name,
        this.points,
        this.sex,
        this.createdAt,
        this.updatedAt});

  DataProfile.fromJson(Map<String, dynamic> json) {
    areaCode = json['area_code'];
    phoneNumber = json['phone_number'];
    email = json['email'];
    name = json['name'];
    points = json['points'];
    sex = json['sex'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['area_code'] = this.areaCode;
    data['phone_number'] = this.phoneNumber;
    data['email'] = this.email;
    data['name'] = this.name;
    data['points'] = this.points;
    data['sex'] = this.sex;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}