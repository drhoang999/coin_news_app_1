import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:dio/dio.dart';

import 'package:get/get.dart' as get2;

import 'package:coinnews12h/component/dialog/dialog.dart';
import 'package:coinnews12h/load_data/load_firebase.dart';
import 'package:coinnews12h/navigator/navigator.dart';
import 'package:coinnews12h/utils/user_info.dart';



/// Inteceptor which used in Dio to add authentication
/// token, device code before perform any request
///
class AuthInterceptor extends InterceptorsWrapper {
  AuthInterceptor();
  DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) async {

    String deviceId;
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfoPlugin.androidInfo;
      deviceId = androidInfo.androidId;
    } else {
      IosDeviceInfo iosInfo = await deviceInfoPlugin.iosInfo;
      deviceId = iosInfo.identifierForVendor;
    }


    if (UserInfo().getToken() != null) {
      options.headers.putIfAbsent("customer-token", () => UserInfo().getToken());
    }




    options.headers.putIfAbsent("device-id", () => deviceId);

    print('Link: ${options.uri.toString()}');
    print('Header: ${options.headers}');
    printWrapped('Request: ${options.data}');

    var hasImage = false;
    if(options.data is Map)
    {
      (options.data as Map).values.forEach((element) {
        if(element is MultipartFile) {
          hasImage = true;
        }
      });
    }
    if (options.method == 'POST' && hasImage) {
      options.data = new FormData.fromMap(options.data);
    }

    return super.onRequest(options, handler);
  }

  @override
  void onError(DioError error, ErrorInterceptorHandler handler) {
    print('On Error${error.response}');

    if (error is DioError) {
      var dioError = error;
      switch (dioError.type) {
        case DioErrorType.cancel:
          error.error = 'Disconnected';
          break;
        case DioErrorType.connectTimeout:
          error.error = 'Unable to connect to server';
          break;
        case DioErrorType.receiveTimeout:
          error.error = 'Unable to receive data from server';
          break;
        case DioErrorType.response:
          if (dioError.response?.statusCode == 429) {
            error.error = 'You sent too many requests, please try again in 1 minute';
          }
          error.error =
              '${dioError.response?.data["msg"] != null ? dioError.response?.data["msg"] : "Có lỗi xảy ra"}';
          break;
        case DioErrorType.sendTimeout:
          error.error = 'Unable to send data to server';
          break;
        case DioErrorType.other:
          error.error = error.message;
          break;
      }
    }

    return handler.next(error);
  }

  void printWrapped(String text) {
    final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
    pattern.allMatches(text).forEach((match) => print(match.group(0)));
  }

  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) {
    print('------Response: ${response.data}');

    if (response.data["code"] == 401) {
      UserInfo().setToken(null);
      UserInfo().logout();
      SahaDialogApp.showDialogNotificationOneButton(
          mess: "End of login session, please login again",
          barrierDismissible: false,
          onClose: () {
           // get2.Get.offAll(NavigatorScreen());
          });
    }

    if (response.data != null && response.data["success"] == false) {
      throw response.data["msg"] ?? "Error";
    }

    if (response.data != null &&
        response.data["code"] != 200 &&
        response.data["code"] != 400 &&
        response.data["code"] != 500) {
      try {
        if (response.data["code"] == 401) {
          print("Logout");
        } else {}
      } catch (e) {
        print(e.toString());
      }
      return super.onResponse(response, handler);
    }

    return super.onResponse(response, handler);
  }
}
