
import 'package:coinnews12h/data/remote/response-request/config/config_repository.dart';

import 'config_link/profile_repository.dart';
import 'device_token/device_token_repository.dart';
import 'favorite/favorite_repository.dart';
import 'form_bonus/form_bonus.dart';
import 'image/image_repository.dart';
import 'login/login_repository.dart';
import 'otp/otp_repository.dart';
import 'post/post_repository.dart';
import 'profile/profile_repository.dart';
import 'register/register_repository.dart';
import 'texttospeech/texttospeech.dart';

class RepositoryManager {

  static ConfigRepository get configRepository =>
      ConfigRepository();
  static RegisterRepository get registerRepository => RegisterRepository();
  static LoginRepository get loginRepository => LoginRepository();
  static ImageRepository get imageRepository => ImageRepository();
  static DeviceTokenRepository get deviceTokenRepository =>
      DeviceTokenRepository();
  static ProfileRepository get profileRepository => ProfileRepository();
  static ConfigLinkRepository get configLinkRepository => ConfigLinkRepository();
  static FavoriteRepository get favoriteRepository => FavoriteRepository();
  static TextToSpeechRepository get textToSpeechRepository =>
      TextToSpeechRepository();
  static PostRepository get postRepository => PostRepository();
  static OtpRepository get otpRepository => OtpRepository();
  static FormBonusRepository get formBonusRepository => FormBonusRepository();


}
