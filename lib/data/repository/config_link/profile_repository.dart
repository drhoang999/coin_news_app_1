import 'package:coinnews12h/data/remote/response-request/config_link/config_link.dart';
import 'package:coinnews12h/data/remote/response-request/profile/profile_response.dart';
import 'package:coinnews12h/data/remote/saha_service_manager.dart';
import 'package:coinnews12h/data/repository/handle_error.dart';

class ConfigLinkRepository {
  Future<List<ConfigLink>?> getLink() async {
    try {
      var res = await SahaServiceManager().service!.getConfigLink();
      return res.data;
    } catch (err) {
      handleError(err);
    }
  }
}
