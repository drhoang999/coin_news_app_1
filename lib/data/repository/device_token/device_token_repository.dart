import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:coinnews12h/data/remote/response-request/device_token/device_token_user_response.dart';
import 'package:coinnews12h/data/remote/saha_service_manager.dart';
import 'package:coinnews12h/load_data/load_firebase.dart';

import '../handle_error.dart';

class DeviceTokenRepository {
  Future<DataNoti?> updateDeviceTokenUser(String? deviceToken, bool? active) async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    String deviceId;
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceId = androidInfo.androidId;
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceId = iosInfo.identifierForVendor;
    }

    try {
      var res = await SahaServiceManager().service!.updateDeviceTokenUser({
        "device_token": deviceToken,
        "device_id": deviceId,
        // "device_type": Platform.isAndroid ? 0 : 1,
        "active":active
      });

      return res.data;
    } catch (err) {
      handleError(err);
    }
  }


  Future<DataNoti?> getDeviceTokenUser() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    String deviceId;
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceId = androidInfo.androidId;
    } else {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceId = iosInfo.identifierForVendor;
    }

    deviceId = FCMToken().token!;

    try {
      var res = await SahaServiceManager().service!.getDeviceTokenUser(
          deviceId
      );

      return res.data;
    } catch (err) {
      handleError(err);
    }
  }
}
