import 'package:coinnews12h/data/remote/response-request/auth/register_response.dart';
import 'package:coinnews12h/data/remote/saha_service_manager.dart';

import '../handle_error.dart';

class RegisterRepository {
  Future<DataRegister?> register({String? phone, String? pass, String ?email, String? name}) async {
    try {
      var res = await SahaServiceManager().service!.register({
        "phone_number": phone,
        "password": pass,
        "name":name,
        "email":email
      });
      return res.data;
    } catch (err) {
      handleError(err);
    }
  }

}
